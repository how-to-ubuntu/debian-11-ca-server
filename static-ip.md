# Descobrir o IP do server Debian 11

```sh
hostname -I | cut -d ' ' -f 1
```

<br>

# Fixar IP no server Debian 11

Em uma instalação limpa, o arquivo `/etc/network/interfaces` deve estar assim:

`~cat /etc/network/interfaces`
![img/debian-11-ca-server-etc-network-interfaces.png](./img/debian-11-ca-server-etc-network-interfaces.png)

<br>

Utilizando o editor de textos __nano__, vou fixar o IP do server, alterando o conteúdo do arquivo `/etc/network/interfaces`.

Para editar o arquivo:

```sh
sudo nano /etc/network/interfaces
```

![img/debian-11-ca-server-static-ip.png](./img/debian-11-ca-server-static-ip.png)

<br>
