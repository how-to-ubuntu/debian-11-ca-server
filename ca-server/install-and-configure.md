# Como instalar e configurar uma autoridade de certificação (CA) no Debian 11

Fonte: https://www.digitalocean.com/community/tutorials/how-to-set-up-and-configure-a-certificate-authority-ca-on-debian-10-pt

<br>

A premissa agora, é que temos um [Debian 11 em uma máquina virtual com o VirtualBox](https://gitlab.com/how-to-ubuntu/debian-11-server-virtual-machine) e foi [configurado o IP estático para o server](https://gitlab.com/how-to-ubuntu/debian-11-ca-server/-/blob/main/static-ip.md#fixar-ip-no-server-debian-11)

O IP utilizado para este exemplo será o `192.168.15.3`

<br>

## Incluir um host conhecido

Editar o arquivo `/etc/hosts` e incluir as linhas

Para editar com o __nano__

```sh
sudo nano /etc/hosts
```

<br>

Incluir as linhas

```sh
# CA Server Debian 11
192.168.15.3    ca-server
```

> Ctrl + s para salvar

> Ctrl + x para sair

<br>

## Acessar o CA Server via SSH

```sh
ssh vin@ca-server
```

<br>

No primeiro acesso será solicitada uma confirmação para continuar a conexão e adicionar o `ca-server` na lista de hosts conhecidos.

Basta digitar `yes` e depois teclar `Enter`

![img/debian-11-ca-server-add-know-host.png](./img/debian-11-ca-server-add-know-host.png)

<br>

Em seguida, informar a senha do usuário `vin`

<br>

## Instalar o Easy-RSA

```sh
sudo apt update && sudo apt install -y easy-rsa
```

<br>

## Preparar diretório de infraestrutura de chaves públicas

```sh
mkdir ~/easy-rsa && \
ln -s /usr/share/easy-rsa/* ~/easy-rsa/ && \
chmod 700 /home/vin/easy-rsa && \
cd ~/easy-rsa && \
./easyrsa init-pki

```

<br>

## Criar  autoridade de certificação

Criar o arquivo `vars`

```sh
cd ~/easy-rsa && \
nano vars
```

<br>

Incluir essas linhas no arquivo `vars`

<pre>
set_var EASYRSA_REQ_COUNTRY    "BR"
set_var EASYRSA_REQ_PROVINCE   "Goiás"
set_var EASYRSA_REQ_CITY       "Goiania"
set_var EASYRSA_REQ_ORG        "Instate"
set_var EASYRSA_REQ_EMAIL      "vinicius@viniciusalopes.com.br"
set_var EASYRSA_REQ_OU         "Vovolinux"
</pre>

<br>



```sh
./easyrsa init-pki && \
./easyrsa build-ca nopass
```












