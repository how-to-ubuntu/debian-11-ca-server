# Configurar firewall básico para SSH

```sh
sudo apt update && \
sudo apt install -y ufw && \
sudo ufw allow OpenSSH && \
sudo ufw enable && \
sudo ufw status numbered
```
<br>

