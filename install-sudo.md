# Instalar sudo no Debian 11

Logar como r00t

```sh
su -
```

<br>

Instalar o __sudo__

```sh
apt install -y sudo
```

<br>

Incluir o usuário comum no sudoers, para permitir a utilização do __sudo__

```sh
usermod -aG sudo vin
```

<br>

Executar o comando `exit` para encerrar a sessão do r00t e depois, `exit` novamente para encerrar a sessão do usuário *vin*.

Logando novamente com o usuário, o comando `sudo` já será permitido para o usuário adicionado (*vin*)

<br>
