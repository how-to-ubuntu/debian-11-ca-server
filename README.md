# debian-11-ca-server

Este projeto mostra como instalar e configurar uma autoridade de certificação (CA) no Debian 11

## Getting started

A premissa aqui é que eu já tenho uma instalação limpa do [Debian 11 em uma máquina virtual com o __VirtualBox__](https://gitlab.com/how-to-ubuntu/debian-11-server-virtual-machine).

Também deve-se observar e levar em consideração a máscara de rede que está sendo utilizada na minha rede interna (255.255.255.0) e que o meu `gateway padrão` é `192.168.15.1`

<br>























